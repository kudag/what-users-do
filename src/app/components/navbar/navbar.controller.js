/** @ngInject */
function wudNavbarController($state) {
    var navbar = this;

    //build navbar items from defined states
    navbar.menuItems = $state.get().filter(function (item) {
        return !item.abstract;
    }).map(function (s) {
        return {
            url: s.url,
            stateName: s.name,
            title: s.title
        }
    });
}

module.exports = wudNavbarController;
