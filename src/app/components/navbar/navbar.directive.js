/** @ngInject */
function wudNavbar() {
    return {
        restrict: 'E',
        template: require('./navbar.html'),
        controller: require('./navbar.controller.js'),
        controllerAs: 'navbar'
    };
}

module.exports = wudNavbar;
