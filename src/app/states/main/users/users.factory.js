'use strict';

/** @ngInject */
function UsersFactory($http, EnviromentConfig) {
    return {
        getUsers: function (){
            return $http({
                url: EnviromentConfig.server.url + 'users', method: 'GET'
            });
        },
        addUser: function (userData) {
            return $http({
                url: EnviromentConfig.server.url + 'users', method: 'POST', data: userData
            });
        }
    };
}
module.exports = UsersFactory;
