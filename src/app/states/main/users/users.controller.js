'use strict';
/** @ngInject */

function UsersController(UsersFactory, $log) {
    var users = this;
  
    users.waitingForServer = false;
    users.errorMsg = '';
    users.successMsg = '';
    users.allUsers = [];
    users.user = {};
    
    //get users
    users.getUsers = function () {
        users.waitingForServer = true;

        UsersFactory.getUsers().then(function (resp) {            
            if ([200, 201].indexOf(resp.status) > -1) {                
                users.errorMsg = '';
                users.allUsers = resp.data;

                users.errorMsg = '';
                users.waitingForServer = false;
            }
        }).catch(function (errorResp) {
            users.waitingForServer = false;
            users.errorMsg = `An error occurred. ${errorResp.data? errorResp.data : ''}`;
        });
    };

    //add users
    users.addUser = function (event) {
        event.preventDefault();
        users.successMsg = users.errorMsg = '';
        users.waitingForServer = true;

        UsersFactory.addUser(users.user).then(function (resp) {
            if ([200, 201].indexOf(resp.status) > -1) {
                users.successMsg = `User successfully added. ${resp.data}`;
                users.getUsers();

                //clear form fields
                users.user.firstname = '';
                users.user.lastname = '';
                users.user.email = '';
                users.errorMsg = '';
                users.waitingForServer = false;
            }
        }).catch(function (errorResp) {
            users.waitingForServer = false;
            users.errorMsg = `An error occurred. ${errorResp.data ? errorResp.data : ''}`;
        });
    };

    users.getUsers();
}
module.exports = UsersController;
