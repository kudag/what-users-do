var ngModule = angular
  .module('wud.techtest.states.main.users', [])
  .factory('UsersFactory', require('./users.factory.js'))
  .controller('UsersController', require('./users.controller.js'));
module.exports = ngModule.name;
